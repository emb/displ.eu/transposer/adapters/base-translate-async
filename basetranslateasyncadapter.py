# Adapter Captions Base Class


import base_adapter
from confluent_kafka import Consumer
import json
from threading import Thread
import random
import webvtt
from webvtt import WebVTT, Caption
import copy
import uuid
from deepmerge import always_merger
import time
from bs4 import BeautifulSoup
from lxml import etree
import logging

# self-written modules
import html_parser
from adapter_exceptions import LanguageNotSupportedException

LIST_TOPIC_ID_PREFIX = "translate_list_"
TEXT_TOPIC_ID_PREFIX = "translate_text_"

RATE_ACCEPTED_FAIL_TRANSLATIONS = 1

# how long we should wait until we give up on getting any more translations back
# if this is parallelised then we just need to concider what the longest time would be for one translation
TIMEOUT_FOR_TRANSLATIONS_IN_SECONDS = "timeout_for_translations_in_seconds"
TIMEOUT_FOR_TRANSLATIONS_IN_SECONDS_DEFAULT = 10 # This is low for development so we notice the timeout for prodction we use a much highet number
                                                 # because we translate huge chunks in one go for now (hopefully we will parellelize more in the future) 

class TranslateAsyncException(Exception):
    pass

class BaseTranslateAsyncAdapter(base_adapter.BaseAdapter):
    def __init__(self, connector, config_path):
        super(BaseTranslateAsyncAdapter, self).__init__(connector, config_path)
        self.timeout_for_translations_in_seconds = 10
        if self.config.get("baseasynctranslateadapter") is not None:
            self.timeout_for_translations_in_seconds = self.config.get("baseasynctranslateadapter").get(TIMEOUT_FOR_TRANSLATIONS_IN_SECONDS, 10)

    def consume_translations(self, topic_id, translations, length):
        # TODO check out each of these params in the config and understand them
        consumer_config = always_merger.merge({
                'client.id': str(uuid.uuid4()),
                'group.id': str(uuid.uuid4()),
                'default.topic.config': {'auto.offset.reset': 'smallest'},
                'on_commit': self.connector._commit_completed,
                'error_cb': self.connector._consumer_error_cb
            },
            self.connector.config.get('kafkaConnectCommon').options if self.connector.config.has('kafkaConnectCommon') else {}
        )

        consumer = Consumer(consumer_config)
        self.connector.createTopics(topic_id)
        consumer.subscribe([topic_id])
        sum = 0
        epoch_time_in_seconds = time.time()
        while sum != length and epoch_time_in_seconds + self.timeout_for_translations_in_seconds > time.time():
            msg = consumer.poll(timeout=1.0)

            if msg is None: continue

            epoch_time_in_seconds = time.time() # reset the time - probably better without when we parellelize
            payload = None
            try:
                payload = msg.value()
                payload = payload.decode("utf-8")
            except:
                pass
            if isinstance(payload, str):
                try:
                    payload = json.loads(payload)
                except:
                    pass
            
            logging.info(f"payload after decoding and parsing: {payload}")

            language_id = payload["language_id"] # call this a unique id so it's the same for captions TODO
            if topic_id.startswith(TEXT_TOPIC_ID_PREFIX):
                if translations[language_id] is None:
                    translations[language_id] = payload
                    sum += 1  # only increment this when it is actually the first (and hopefully only) for this caption of this language
                else:
                    if "status" not in payload or payload["status"] == 200:
                        translations[language_id] = payload  # take the last one for now and don't make a list because we wouldn't know what to do with a list anyway.
                        logging.error(f"Translation already recieved for this with language {language_id}")
                    else:
                        error = payload["message"]
                        logging.error(f"One error already recieved! language {language_id} contains this error: {error}")
            elif topic_id.startswith(LIST_TOPIC_ID_PREFIX):
                index = payload["index"]
                if translations[language_id][index] is None:
                    translations[language_id][index] = payload
                    sum += 1  # only increment this when it is actually the first (and hopefully only) for this index of this language
                else:
                    if "status" not in payload or payload["status"] == 200:
                        translations[language_id][
                            index
                        ] = payload  # take the last one for now and don't make a list because we wouldn't know what to do with a list anyway.
                        logging.error(f"Translation already recieved for this index {index} with language {language_id}")
                    else:
                        error = payload["message"]
                        logging.error(f"One error already recieved! index {index}, language {language_id} contains this error: {error}")

        self.translation_result_success = True
        consumer.close()

    # gets a vtt file and returns a vtt files
    def translate_captions(self, file_name, source_language_id, target_language_ids):
        # hash the file then persist to database so you can check if such a job proccess is already happening. - for future use case when we save data in the db
        response_topic_id = f"{LIST_TOPIC_ID_PREFIX}{random.getrandbits(128)}"
        vtt_source_language = webvtt.read(file_name)
        translations = {}
        translations_as_vtt = {}
        amount_of_captions = len(vtt_source_language)
        amount_of_requests = amount_of_captions * len(target_language_ids)
        self.connector.createTopics(response_topic_id)
        self.translation_result_success = False
        for target_language_id in target_language_ids:
            translations[target_language_id] = {}
            translations_as_vtt[target_language_id] = {}
            translations_as_vtt[target_language_id]["sum_successful_translations"] = 0
            translations_as_vtt[target_language_id]["payload"] = WebVTT()
            for index, caption_source_language in enumerate(vtt_source_language):
                translations[target_language_id][index] = None

        # Initiate collector for translations
        thread = Thread(
            target=self.consume_translations,
            args=(
                response_topic_id,
                translations,
                amount_of_requests,
            ),
        )
        # Start collector thread
        thread.start()

        # Initiate translation requests
        for index, caption_source_language in enumerate(vtt_source_language):
            # check what happens with multiple lines - make sure they get translated together as to not lose context (transles better with context)
            # perhaps think about translating the whole file as one because that also might be important for context and also is an interesting different approach
            for target_language_id in target_language_ids:
                # trigger translation
                request_payload = {
                    "response_topic_id": response_topic_id,
                    "input": caption_source_language.text,
                    "source_language_id": source_language_id,
                    "target_language_id": target_language_id,
                    "index": index,
                }
                if not (target_language_id in self.translation_target_topics and self.translation_target_topics[target_language_id]):
                    raise LanguageNotSupportedException(f"translation targets is missing {target_language_id}")
                request_payload["topic"] = self.translation_target_topics[target_language_id]
                self.connector.sendResponseTo(request_payload)

                # prepare vtt files with captions for every language
                caption = Caption(
                    caption_source_language.start,
                    caption_source_language.end,
                    "",  # prepare all caption objects but with empty text to fill in after the translations come back
                )
                translations_as_vtt[target_language_id]["payload"].captions.append(caption)

        # Wait for all translations to come back
        thread.join()

        at_least_one_success = False
        for language_id in translations:
            for index, caption_translation in enumerate(translations_as_vtt[language_id]["payload"]):
                if translations[language_id][index] is None:
                    raise TranslateAsyncException(f"something went wrong - amount_of_requests: {amount_of_requests}, response_topic_id: {response_topic_id}")
                if (
                    "status" not in translations[language_id][index]
                    or translations[language_id][index]["status"] == 200
                ):
                    caption_translation.text = translations[language_id][index]["translated_text"]
                    translations_as_vtt[language_id]["sum_successful_translations"] += 1
                else:
                    payload = translations[language_id][index]
                    error = payload["message"]
                    logging.error(f"caption {index}, language {language_id} contains this error: {error}")
            language_success = translations_as_vtt[language_id]["sum_successful_translations"]/amount_of_captions >= RATE_ACCEPTED_FAIL_TRANSLATIONS
            translations_as_vtt[language_id]["success"] = language_success
            if language_success:
                at_least_one_success = True
            else:
                # don't end up sending an over complicated json object back to the client
                del translations_as_vtt[language_id]["payload"]

        if not at_least_one_success:
            self.translation_result_success = False
        return translations_as_vtt

    def translate_html(self, html, source_language_id, target_language_ids):
        parser = html_parser.HtmlParser(html)
        text_list = parser.get_text_content()

        response_topic_id = f"{LIST_TOPIC_ID_PREFIX}{random.getrandbits(128)}"
        translations = {}
        translations_result = {}
        self.connector.createTopics(response_topic_id)
        amount_of_requests = 0
        for target_language_id in target_language_ids:
            translations[target_language_id] = {}
            translations_result[target_language_id] = {}
            translations_result[target_language_id]["sum_successful_translations"] = 0
            translations_result[target_language_id]["payload"] = copy.deepcopy(text_list)
            index = 0
            for list_of_elements in text_list:
                for element in list_of_elements:
                    if element:
                        translations[target_language_id][index] = None
                        amount_of_requests += 1
                    else:
                        translations[target_language_id][index] = element
                    index += 1

        # Initiate collector for translations
        thread = Thread(
            target=self.consume_translations,
            args=(
                response_topic_id,
                translations,
                amount_of_requests,
            ),
        )
        # Start collector thread
        thread.start()

        # Initiate translation requests
        index = 0
        for list_of_elements in text_list:
            for element in list_of_elements:
                for target_language_id in target_language_ids:
                    if element:
                        # trigger translation
                        request_payload = {
                            "response_topic_id": response_topic_id,
                            "input": element,
                            "target_language_id": target_language_id,
                            "index": index,
                        }
                        if source_language_id:
                            request_payload["source_language_id"] = source_language_id
                        if not (target_language_id in self.translation_target_topics and self.translation_target_topics[target_language_id]):
                            raise LanguageNotSupportedException(f"translation targets is missing {target_language_id}")
                        request_payload["topic"] = self.translation_target_topics[target_language_id]
                        self.connector.sendResponseTo(request_payload)
                index += 1


        # Wait for all translations to come back
        thread.join()

        at_least_one_success = False
        # use this index to put the translation in the right place
        for language_id in translations:
            index = 0
            for index_list_of_elements, list_of_elements in enumerate(translations_result[language_id]["payload"]):
                for index_list_element, element in enumerate(list_of_elements):
                    if translations[language_id][index] is None:
                        raise TranslateAsyncException(f"something went wrong - amount_of_requests: {amount_of_requests}, response_topic_id: {response_topic_id}")
                    if (
                        "status" not in translations[language_id][index]
                        or translations[language_id][index]["status"] == 200
                    ):
                        if translations[language_id][index]:
                            list_of_elements[index_list_element] = translations[language_id][index]["translated_text"]
                            translations_result[language_id]["sum_successful_translations"] += 1
                    else:
                        payload = translations[language_id][index]
                        error = payload["message"]
                        logging.error(f"element {index}, language {language_id} contains this error: {error}")
                        list_of_elements[index_list_element] = text_list[index_list_of_elements][index_list_element] # get original text (wrapped by a list)
                    index += 1
            language_success = translations_result[language_id]["sum_successful_translations"]/(amount_of_requests/len(target_language_ids)) >= RATE_ACCEPTED_FAIL_TRANSLATIONS # TODO should not be more than one!!!
            translations_result[language_id]["success"] = language_success
            if language_success:
                at_least_one_success = True
                translations_result[language_id]["text"] = parser.get_result(translations_result[language_id]["payload"])
            del translations_result[language_id]["payload"]

        if not at_least_one_success:
            self.translation_result_success = False
        return translations_result

    def translate_text(self, text, source_language_id, target_language_ids):
        response_topic_id = f"{TEXT_TOPIC_ID_PREFIX}{random.getrandbits(128)}"
        translations = {}
        self.connector.createTopics(response_topic_id)
        for target_language_id in target_language_ids:
            translations[target_language_id] = None

        # Initiate collector for translations
        thread = Thread(
            target=self.consume_translations,
            args=(
                response_topic_id,
                translations,
                len(target_language_ids),
            ),
        )
        # Start collector thread
        thread.start()

        # Initiate translation requests
        for target_language_id in target_language_ids:
            # trigger translation
            request_payload = {
                "response_topic_id": response_topic_id,
                "input": text,
                "target_language_id": target_language_id,
            }
            if source_language_id:
                request_payload["source_language_id"] = source_language_id
            if not (target_language_id in self.translation_target_topics and self.translation_target_topics[target_language_id]):
                raise LanguageNotSupportedException(f"translation targets is missing {target_language_id}")
            request_payload["topic"] = self.translation_target_topics[target_language_id]
            self.connector.sendResponseTo(request_payload)

        # Wait for all translations to come back
        thread.join()

        at_least_one_success = False
        for language_id in translations:
            if translations[language_id] is None or "translated_text" not in translations[language_id]:
                logging.error(f"language id: {language_id} is None, or it's missing translated_text, which means it probably failed to translate")
                logging.error(f"Check if the adapter behind the topic: {self.translation_target_topics[language_id]} is running")
                logging.error(f"otherwise something else went wrong, maybe this helps: amount_of_requests: {len(target_language_ids)}, response_topic_id: {response_topic_id}")
                translations[language_id] = {
                    "success": False
                }
            elif (
                "status" not in translations[language_id]
                or translations[language_id]["status"] == 200
            ):
                translations[language_id] = {
                    "text": translations[language_id]["translated_text"],
                    "success": True
                }
                at_least_one_success = True
            else:
                payload = translations[language_id]
                error = payload["message"]
                logging.error(f"language {language_id} contains this error: {error}")
                translations[language_id] = {
                    "success": False
                }
        if not at_least_one_success:
            self.translation_result_success = False
        return translations
    
    def translate_transcription_xml(self, transcrption, source_language_id, target_language_ids):
        # Transcription to xml
        if isinstance(transcrption, str):
            try:
                transcrption = json.loads(transcrption)
            except:
                pass
        xml_text = self.convert_transcription_to_xml(transcrption)
        response_topic_id = f"{TEXT_TOPIC_ID_PREFIX}{random.getrandbits(128)}"
        translations = {}
        self.connector.createTopics(response_topic_id)
        for target_language_id in target_language_ids:
            translations[target_language_id] = None

        # Initiate collector for translations
        thread = Thread(
            target=self.consume_translations,
            args=(
                response_topic_id,
                translations,
                len(target_language_ids),
            ),
        )
        # Start collector thread
        thread.start()

        # Initiate translation requests
        for target_language_id in target_language_ids:
            # trigger translation
            request_payload = {
                "markup":"xml",
                "response_topic_id": response_topic_id,
                "input": xml_text,
                "target_language_id": target_language_id,
            }
            if source_language_id:
                request_payload["source_language_id"] = source_language_id
            if not (target_language_id in self.translation_target_topics and self.translation_target_topics[target_language_id]):
                raise LanguageNotSupportedException(f"translation targets is missing {target_language_id}")
            request_payload["topic"] = self.translation_target_topics[target_language_id]
            self.connector.sendResponseTo(request_payload)

        # Wait for all translations to come back
        thread.join()

        at_least_one_success = False

        for language_id in translations:
            if translations[language_id] is None or "translated_text" not in translations[language_id]:
                logging.error(f"language id: {language_id} is None, or it's missing translated_text, which means it probably failed to translate")
                logging.error(f"Check if the adapter behind the topic: {self.translation_target_topics[language_id]} is running")
                logging.error(f"otherwise something else went wrong, maybe this helps: amount_of_requests: {len(target_language_ids)}, response_topic_id: {response_topic_id}")
                translations[language_id] = {
                    "success": False
                }
            elif (
                "status" not in translations[language_id]
                or translations[language_id]["status"] == 200
            ):
                translated_segments_list = self.extract_transcription_translation_from_xml(translations[language_id]["translated_text"])
                segments = transcrption["segments"]
                if len(segments) == len(translated_segments_list): 
                    translations_result = []
                    # Initiate translation requests
                    for index, segment in enumerate(segments):
                        segment_copy = {}
                        segment_copy["id"] = segment["id"]
                        segment_copy["start"] = segment["start"]
                        segment_copy["end"] = segment["end"]
                        segment_copy["text"] = translated_segments_list[index]
                        translations_result.append(segment_copy)

                    translations[language_id] = {
                        "payload": translations_result,
                        "success": True
                    }
                    at_least_one_success = True
                else:
                    translations[language_id] = {
                        "success": False
                    }
            else:
                payload = translations[language_id]
                error = payload["message"]
                logging.error(f"language {language_id} contains this error: {error}")
                translations[language_id] = {
                    "success": False
                }
        if not at_least_one_success:
            self.translation_result_success = False
        return translations

    def translate_xliff(self, xliff_file_path, source_language_id, target_language_ids):
        tree = etree.parse(xliff_file_path)



        root = tree.getroot()
        file_element = root.find('.//{urn:oasis:names:tc:xliff:document:1.2}file')

        trans_units = file_element.findall('.//{urn:oasis:names:tc:xliff:document:1.2}trans-unit')
        for trans_unit in trans_units:
            trans_unit.find('.//{urn:oasis:names:tc:xliff:document:1.2}target').text = None
        # hash the file then persist to database so you can check if such a job proccess is already happening. - for future use case when we save data in the db
        response_topic_id = f"{LIST_TOPIC_ID_PREFIX}{random.getrandbits(128)}"
        translations = {}
        translations_as_xliff = {}
        amount_of_trans_units = len(trans_units)
        amount_of_requests = amount_of_trans_units * len(target_language_ids)
        self.connector.createTopics(response_topic_id)
        self.translation_result_success = False
        for target_language_id in target_language_ids:
            translations[target_language_id] = {}
            translations_as_xliff[target_language_id] = {}
            translations_as_xliff[target_language_id]["sum_successful_translations"] = 0
            translations_as_xliff[target_language_id]["payload"] = copy.deepcopy(tree)
            for index, trans_unit in enumerate(trans_units):
                translations[target_language_id][index] = None



        # Initiate collector for translations
        thread = Thread(
            target=self.consume_translations,
            args=(
                response_topic_id,
                translations,
                amount_of_requests,
            ),
        )
        # Start collector thread
        thread.start()


        for index, trans_unit in enumerate(trans_units):
            source_text = trans_unit.find('.//{urn:oasis:names:tc:xliff:document:1.2}source').text
            for target_language_id in target_language_ids:
                # trigger translation
                request_payload = {
                    "response_topic_id": response_topic_id,
                    "input": source_text,
                    "source_language_id": source_language_id,
                    "target_language_id": target_language_id,
                    "index": index,
                    "markup": "html"
                }
                if not (target_language_id in self.translation_target_topics and self.translation_target_topics[target_language_id]):
                    raise LanguageNotSupportedException(f"translation targets is missing {target_language_id}")
                request_payload["topic"] = self.translation_target_topics[target_language_id]
                self.connector.sendResponseTo(request_payload)

        # Wait for all translations to come back
        thread.join()

        at_least_one_success = False
        for language_id in translations:
            root_translations = translations_as_xliff[language_id]["payload"].getroot()
            file_element_translations = root_translations.find('.//{urn:oasis:names:tc:xliff:document:1.2}file')

            trans_units_translations = file_element_translations.findall('.//{urn:oasis:names:tc:xliff:document:1.2}trans-unit')

            for index, trans_unit_translation in enumerate(trans_units_translations):
                if translations[language_id][index] is None:
                    raise TranslateAsyncException(f"something went wrong - amount_of_requests: {amount_of_requests}, response_topic_id: {response_topic_id}")
                if (
                    "status" not in translations[language_id][index]
                    or translations[language_id][index]["status"] == 200
                ):
                    trans_unit_translation.find('.//{urn:oasis:names:tc:xliff:document:1.2}target').text = translations[language_id][index]["translated_text"]
                    translations_as_xliff[language_id]["sum_successful_translations"] += 1
                else:
                    payload = translations[language_id][index]
                    error = payload["message"]
                    logging.error(f"trans unit {index}, language {language_id} contains this error: {error}")
            language_success = translations_as_xliff[language_id]["sum_successful_translations"]/amount_of_trans_units >= RATE_ACCEPTED_FAIL_TRANSLATIONS
            translations_as_xliff[language_id]["success"] = language_success
            if language_success:
                at_least_one_success = True
            else:
                # don't end up sending an over complicated json object back to the client
                del translations_as_xliff[language_id]["payload"]

        if not at_least_one_success:
            self.translation_result_success = False
        return translations_as_xliff

    def convert_transcription_to_xml(self, transcription):
        xml_text = ""
        if transcription["segments"]:
            for i, segment in enumerate(transcription["segments"]):
                xml_text += f'<x id="{i}">{segment["text"]}</x>' + " "
        return xml_text.rstrip()
    
    def extract_transcription_translation_from_xml(self, translated_xml_text):
        soup = BeautifulSoup(translated_xml_text, 'lxml')
        extracted_texts = [tag.get_text(strip=True) for tag in soup.find_all('x')]
        return extracted_texts
        
        
        # we need to convert the webvtt file into an object and then copy it into a few other
        # objects for the other languages -      then we need to use pieces in the
        # translate_transcription_xml to send everything as an xml instead of line by line,
        # maybe some similiar finction to convert_transcription_to_xml but this time using the vtt object
        # and at the end something similiar to extract_transcription_translation_from_xml but to vtt, 
        # maybe first converting to the transcroption format of whisper so we can use whisper_translations_output_to_vtt function?
        
        
    def translate_captions_xml(self, file_name, source_language_id, target_language_ids):
        # Initial setup from original translate_captions
        response_topic_id = f"{TEXT_TOPIC_ID_PREFIX}{random.getrandbits(128)}"  # Using TEXT prefix since it's one message per language
        vtt_source_language = webvtt.read(file_name)
        translations = {}
        translations_as_vtt = {}
        amount_of_captions = len(vtt_source_language)
        amount_of_requests = len(target_language_ids)  # One request per language
        self.connector.createTopics(response_topic_id)
        self.translation_result_success = False
        
        # Initialize translation structures
        for target_language_id in target_language_ids:
            translations[target_language_id] = None
            translations_as_vtt[target_language_id] = {
                "sum_successful_translations": 0,
                "payload": WebVTT(),
                "success": False
            }

        # Convert VTT to XML
        def vtt_to_xml(vtt_obj):
            xml_text = ""
            for i, caption in enumerate(vtt_obj):
                xml_text += f'<x id="{i}" start="{caption.start}" end="{caption.end}">{caption.text}</x> '
            return xml_text.rstrip()

        # Convert translated XML back to VTT-compatible format
        def xml_to_vtt_segments(translated_xml):
            soup = BeautifulSoup(translated_xml, 'lxml')
            segments = []
            for tag in soup.find_all('x'):
                segment = {
                    "id": int(tag.get('id')),
                    "start": tag.get('start'),
                    "end": tag.get('end'),
                    "text": tag.get_text(strip=True)
                }
                segments.append(segment)
            return sorted(segments, key=lambda x: x["id"])  # Ensure correct order

        # Convert segments to VTT
        def segments_to_vtt(segments, vtt_obj):
            for segment in segments:
                caption = Caption(
                    segment["start"],
                    segment["end"],
                    segment["text"]
                )
                vtt_obj.captions.append(caption)
            return vtt_obj

        # Prepare XML input
        xml_input = vtt_to_xml(vtt_source_language)

        # Start collector thread using existing consume_translations
        thread = Thread(
            target=self.consume_translations,
            args=(response_topic_id, translations, amount_of_requests),
        )
        thread.start()

        # Send translation requests
        for target_language_id in target_language_ids:
            if target_language_id not in self.translation_target_topics:
                raise LanguageNotSupportedException(f"translation targets is missing {target_language_id}")
                
            request_payload = {
                "markup": "xml",
                "response_topic_id": response_topic_id,
                "input": xml_input,
                "source_language_id": source_language_id,
                "target_language_id": target_language_id,
                "topic": self.translation_target_topics[target_language_id]
            }
            self.connector.sendResponseTo(request_payload)

        # Wait for translations
        thread.join()

        # Process results
        at_least_one_success = False
        for language_id in translations:
            vtt_output = translations_as_vtt[language_id]["payload"]
            
            if translations[language_id] is None:
                logging.error(f"Translation failed for {language_id}")
                continue
                
            if ("status" not in translations[language_id] or 
                translations[language_id]["status"] == 200):
                try:
                    segments = xml_to_vtt_segments(translations[language_id]["translated_text"])
                    translations_as_vtt[language_id]["payload"] = segments_to_vtt(segments, vtt_output)
                    translations_as_vtt[language_id]["sum_successful_translations"] = amount_of_captions
                    translations_as_vtt[language_id]["success"] = True
                    at_least_one_success = True
                except Exception as e:
                    logging.error(f"Error processing translation for {language_id}: {e}")
            else:
                error = translations[language_id].get("message", "Unknown error")
                logging.error(f"Translation error for {language_id}: {error}")

        self.translation_result_success = at_least_one_success
        return translations_as_vtt